#GETTING STARTED WITH SQL

In this module, you will be introduced to databases. You will create a database instance on the cloud. You will learn some of the basic SQL statements. You will also write and practice basic SQL hands-on on a live database.

**Learning Objectives**
- Create an IBM Cloud Lite account.
- Demonstrate how to write basic SQL statements
- Describe SQL and Databases
- Create and execute CREATE TABLE, SELECT, INSERT, and DELETE SQL statements with a live database

**Hands-on Lab: Simple SELECT Statements**
In this lab you will practice the basics of SQL SELECT Statements using a SQL practice tool called Datasette in the Skills Network Lab environment. 

To download or view just the lab instructions in a different browser tab, click here[](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/labs/Lab%20-%20Basics%20of%20SQL%20SELECT%20Statement/instructional-labs.md.html). 
